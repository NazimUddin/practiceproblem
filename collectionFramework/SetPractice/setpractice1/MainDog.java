package collectionFramework.SetPractice.setpractice1;

public class MainDog {
    public static void main(String[] args) {
        Dog dog = new Dog("Lala");
        Dog dog1 = new Dog("Lala");
        Italian italianDog = new Italian("Lala");
        Italian italianDog1 = new Italian("Lala");
        System.out.println(dog.equals(dog1));
        System.out.println(dog.equals(italianDog));
        System.out.println(italianDog.equals(italianDog1));
    }

}
