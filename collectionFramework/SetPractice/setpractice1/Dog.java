package collectionFramework.SetPractice.setpractice1;

public class Dog {
    private String name;

    public Dog() {
    }
    public Dog(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
