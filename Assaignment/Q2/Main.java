package Assaignment.Q2;

import jdk.swing.interop.SwingInterOpUtils;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Shape shape;

        System.out.println("Press 1 for Circle and Press 2 for Rectangle");
        int no = scanner.nextInt();
        shape = getShape(no);
        System.out.println(shape.toString());
        System.out.println("Perimeter from getPerimeter() method");
        shape.getPerimeter();
    }

    public static Shape getShape(int no) {
        switch (no) {
            case 1:
                return new Circle(12.3);
            case 2:
                return new Rectangle(12.3, 14.3);
        }
        return null;
    }
}
