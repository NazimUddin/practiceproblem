package Assaignment.Q2;

public abstract class Shape {
    private double length;
    private double width;

    Shape(double length, double width) {
        this.length = length;
        this.width = width;
    }

    public abstract void getPerimeter();

    public abstract double getArea();


    public double getLength() {
        return length;
    }

    public double getWidth() {
        return width;
    }
}
