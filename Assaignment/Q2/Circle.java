package Assaignment.Q2;

public class Circle extends Shape {

    Circle(double length) {
        super(length, length);
    }

    @Override
    public void getPerimeter() {
        System.out.printf("%s : %s%n", "length", getLength());
    }

    @Override
    public double getArea() {
        return Math.PI * getLength() * getLength();
    }

    @Override
    public String toString() {
        return String.format("%s : %s%n%s : %.2f%n",
                "Radius", getLength(), "Area", getArea());
    }
}
