package Assaignment.Q2;

public class Rectangle extends Shape {

    Rectangle(double length, double width) {
        super(length, width);
    }

    @Override
    public void getPerimeter() {
        System.out.printf("%s : %s%n%s : %s", "length", getLength(), "Width", getWidth());
    }

    @Override
    public double getArea() {
        return getLength() * getWidth();
    }

    @Override
    public String toString() {
        return String.format("%s : %s%n%s : %s%n%s : %.2f%n",
                "Length", getLength(), "Width", getWidth(), "Area", getArea());
    }
}
