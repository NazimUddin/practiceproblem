package Assaignment.Q4;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter Base Salary: ");
        double baseSalary = scanner.nextDouble();

        System.out.print("Enter hourly Salary: ");
        double hourlyRate = scanner.nextDouble();

        System.out.print("Enter extra rate: ");
        double extraRate = scanner.nextDouble();

        Employee employee = new Employee(baseSalary, hourlyRate, extraRate);
        System.out.print("Your Total Salary is: " + employee.calculateTotalSalary());

    }
}
