package AssignmentTwo;

public class BankAccount {
    private double savings;
    private String accountName;
    private String accountNumber;
    private double currentBalance;
    private String bank;
    private String branchName;

    BankAccount(double savings, String accountName, String accountNumber, double currentBalance, String bank, String branchName) {
        this.savings = savings;
        this.accountName = accountName;
        this.accountNumber = accountNumber;
        this.currentBalance = currentBalance;
        this.bank = bank;
        this.branchName = branchName;
    }

    @Override
    public String toString() {
        return String.format("%n%s: %s%n%s: %s%n%s: %s%n%s: %s%n%s: %s%n%s: %s%n",
                "Savings", savings, "Account Name", accountName, "Account Number", accountNumber, "Current Balance", currentBalance,
                "Bank Name", bank, "Branch Name", branchName);
    }
}
