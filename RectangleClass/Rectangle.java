package RectangleClass;

public class Rectangle {
    private float height;
    private float weight;

    Rectangle() {
        this(1,1);
    }

    Rectangle(float height, float weight) {
        this.height = height;
        this.weight = weight;
    }

    public float getHeight() {
        return height;
    }

    public void setHeight(float height) {
        if (height <= 0.0 || height >= 20.0) {
            throw new IllegalArgumentException("Your entry need to greater then 0.0");
        }
        this.height = height;
    }

    public float getWeight() {
        return weight;
    }

    public void setWeight(float weight) {
        if (weight <= 0.0 || weight >= 20.0) {
            throw new IllegalArgumentException("Your entry need to greater then 0.0");
        }
        this.weight = weight;
    }

    @Override
    public String toString() {
        return "Your height is : "+getHeight()+"\nYour weight is : "+getWeight();
    }
}
