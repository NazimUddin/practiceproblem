package PayableSystem;

public class PayableInterfaceTest {
    public static void main(String[] args) {
        Payable[] payableObjects = new Payable[6];

        for (int i = 0; i < 6; i++) {
            payableObjects[i] = getPayableEmployee(i + 1);
        }
        System.out.println("Invoices and Employees processed polymorphically:");

        printPayableEmployee(payableObjects);

    }

    public static Payable getPayableEmployee(int no) {
        switch (no) {
            case 1:
                return new Invoice("01234", "seat", 2, 375.00);
            case 2:
                return new Invoice("56789", "tire", 4, 79.95);
            case 3:
                return new SalariedEmployee("Asif", "D", "111-11-1111", 800.00, 9, 5, 2020);
            case 4:
                return new CommissionEmployee("kalam", "Ullah", "888-88-8888", 1200.00, 0.5, 9, 7, 2020);
            case 5:
                return new HourlyEmployee("kalam", "Ullah", "888-88-8888", 1200.00, 5, 9, 7, 2020);
            case 6:
                return new BasePlusCommissionEmployee("kalam", "Ullah", "888-88-8888", 1200.00, 0.5, 120.00, 9, 7, 2020);
        }
        return null;
    }

    public static void printPayableEmployee(Payable payables[]) {
        for (Payable currentPayableEmployee : payables) {
            System.out.println(currentPayableEmployee);

            if (currentPayableEmployee instanceof BasePlusCommissionEmployee) {
                BasePlusCommissionEmployee employee = (BasePlusCommissionEmployee) currentPayableEmployee;
                employee.setBaseSalary(1.10 * employee.getBaseSalary());
                System.out.printf("new base salary with 10%% increase is: $%,.2f%n", employee.getBaseSalary());
            }
        }
    }
}
