package iopractice.accountPractice;

import java.io.*;
import java.util.Scanner;

public class CreateSequentialFile {
    public static void main(String[] args) {
        File file = new File("C:\\Users\\Asif\\IdeaProjects\\JavaOOPPractice\\src\\iopractice\\accountPractice\\Simple.txt");
        CreateSequentialFile createSequentialFile = new CreateSequentialFile();
        createSequentialFile.addRecord(file);
        try {
            createSequentialFile.showRecord(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void addRecord(File file){
        if(!file.exists()){
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else {
            Scanner input =   new Scanner(System.in);
            System.out.printf("%s%n%s%n? ", "Enter account number, first name, last name and balance.",
                    "Enter end-of-file indicator to end input.");
            int counter=1;
            while(counter==1){
               // Account record = new Account(input.nextInt(), input.nextLine(), input.nextLine(), input.nextDouble());
                try {
                    BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(file));
                    String l=input.nextLine()+" "+input.nextLine();
                    bufferedWriter.write(l);
                    bufferedWriter.newLine();
                    bufferedWriter.flush();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                counter++;
            }
        }
    }

    public void showRecord(File file) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
        String line;
        while ((line = bufferedReader.readLine())!=null){
            System.out.println(line);
        }
    }
}
