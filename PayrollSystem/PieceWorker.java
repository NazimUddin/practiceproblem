package PayrollSystem;

public class PieceWorker extends Employee {
    private double wage;
    private int pieces;

    PieceWorker(String firstName, String lastName, String socialSecurityNumber, double wage, int pieces) {
        super(firstName, lastName, socialSecurityNumber);
        this.pieces = pieces;
        this.wage = wage;
    }

    @Override
    public double earnings() {
        return wage * pieces;
    }

    @Override
    public String toString() {
        return String.format("PiecesWorker employee: %n%s: %s%n%s: %s%n%s: %s",
                "PieceWorker Employee's wage: ",wage,"PieceWorker Employee's Pieces: ",pieces,"Total Earnings",earnings());
    }
}
