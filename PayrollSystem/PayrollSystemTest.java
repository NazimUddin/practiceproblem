package PayrollSystem;

import java.util.Scanner;

public class PayrollSystemTest {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Employee employee;

        System.out.println("Employees processed individually:");
        for (int i = 1; i <= 5; i++) {
            //int no = scanner.nextInt();
            employee = getEmployee(i);
            System.out.printf("%n%s%n%s: $%,.2f%n%n", employee, "earned", employee.earnings());
        }

        Employee[] employees = new Employee[5];
        for (int i = 0; i < 5; i++) {
            employees[i] = getEmployee(i + 1);
        }

        System.out.printf("Employees processed polymorphically:%n%n");
        printEmployee(employees);

        System.out.println("Print Employee Class:");
        printEmployeeClass(employees);

    }

    public static Employee getEmployee(int no) {
        switch (no) {
            case 1:
                return new SalariedEmployee("John", "Smith", "111-11-1111", 800.00);
            case 2:
                return new HourlyEmployee("Karen", "Price", "222-22-2222", 16.75, 40);
            case 3:
                return new CommissionEmployee(
                        "Sue", "Jones", "333-33-3333", 10000, .06);
            case 4:
                return new BasePlusCommissionEmployee(
                        "Bob", "Lewis", "444-44-4444", 5000, .04, 300);
            case 5:
                return new PieceWorker(
                        "lewis", "Suarez", "555-55-5555", 17.25, 6);
        }
        return null;
    }

    public static void printEmployee(Employee employees[]) {
        for (Employee currentEmployee : employees) {
            System.out.println(currentEmployee);

            if (currentEmployee instanceof BasePlusCommissionEmployee) {
                BasePlusCommissionEmployee employee = (BasePlusCommissionEmployee) currentEmployee;
                employee.setBaseSalary(1.10 * employee.getBaseSalary());
                System.out.printf("new base salary with 10%% increase is: $%,.2f%n", employee.getBaseSalary());
            }
            System.out.printf("earned $%,.2f%n%n", currentEmployee.earnings());
        }
    }

    public static void printEmployeeClass(Employee employees[]) {
        for (int j = 0; j < employees.length; j++)
            System.out.printf("Employee %d is a %s%n", j,
                    employees[j].getClass().getName());
    }
}
